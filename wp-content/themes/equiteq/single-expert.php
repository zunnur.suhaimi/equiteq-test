<?php
/**
 * Template Name: Single Page
 */
get_header();
$id = get_the_ID();
$expert = get_expert($id);
// $industry_expertises = maybe_unserialize($expert->industry_expertise);
?>


    <section>
        <div class="container no-pad-gutters">
            <div class="back mb-4 mb-md-5">
                <i class="fa fa-caret-left align-bottom" style="font-size: 22px;" aria-hidden="true"></i> <a
                        href="/expert" class="btn-outline-success text-uppercase px-0 ml-2">Back to team</a>
            </div>
            <!--May implement the expert's profile here -->
            <div class="row">
                <div class="col-12 col-md-4">
                    <img class="w-100 rounded-circle p-4" src="<?= wp_get_attachment_url($expert->profile_image) ?>"
                         alt="">
                </div>
                <div class="col-12 col-md-8">
                    <h1 class="pb-4"><?= $expert->post_title ?></h1>
                    <h3 class="pb-4"><?= $expert->title ?></h3>
                    <div class="d-flex align-items-center pb-4">
                        <i class="fa fa-map-marker fa-lg text-green" aria-hidden="true"></i>
                        <p class="mb-0 pl-2"><?= get_post($expert->location[0])->post_title ?></p>
                    </div>
                    <div class="d-flex">
                        <div class="px-4">
                            <a href="mailto:<?= $expert->email ?>" target="_blank">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="px-4">
                            <a href="tel:<?= $expert->contact_no ?>" target="_blank">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="px-4">
                            <a href="<?= $expert->linkedin ?>" target="_blank">
                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="pt-4">
                        <?= $expert->post_content ?>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!--May implement the expert's industry expertise here -->

<?php
get_footer();