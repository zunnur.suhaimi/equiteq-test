<?php
/**
 * Template Name: Expert Page
 */
get_header();
$experts = get_experts();
$locations = get_locations();
$industries = get_industries();
?>

<section class="bg-dark-blue">
    <div class="container text-white no-pad-gutters">
        <h3 class="text-uppercase mb-4">
            <?php the_title(); ?>
        </h3>
        <div class="row">
            <div class="col-md-8 mb-4">
                <?php the_content(); ?>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-md-8">
                <h3>FILTERS</h3>
                <div class=" d-block d-md-flex">
                    <label class="pr-4">
                        <select id="industry-filter" class="form-control">
                            <option value="">Sector</option>
                            <?php foreach ($industries as $industry) : ?>
                                <option value="<?= $industry->ID ?>"><?= $industry->post_title ?></option>
                            <?php endforeach; ?>
                        </select>
                    </label>
                    <label>
                        <select id="location-filter" class="form-control">
                            <option value="">Location</option>
                            <?php foreach ($locations as $location) : ?>
                                <option value="<?= $location->ID ?>"><?= $location->post_title ?></option>
                            <?php endforeach; ?>
                        </select>
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <h3>SEARCH</h3>
                <label>
                    <input type="text" id="name-search" class="form-control bg-transparent" >
                </label>
            </div>
        </div>
    </div>
</section>

<section id="experts" class="container">
    <div class="row">
        <?php foreach ($experts as $expert): ?>
            <div class="col-12 col-md-3 p-4 expert-item" data-location="<?= $expert->location[0] ?>"
                 data-industry="<?= $expert->industry_expertise[0] ?>">
                <a class="text-reset text-decoration-none" href="/team/<?= $expert->post_name ?>">
                    <div class="p-4">
                        <img class="w-100 rounded-circle" src="<?= wp_get_attachment_url($expert->profile_image) ?>"
                             alt="">
                    </div>
                    <h3 class="text-center"><?= $expert->post_title ?></h3>
                    <p class="text-center"><?= $expert->title ?></p>
                    <p class="text-center"><i><?= get_post($expert->location[0])->post_title ?></i></p>
                    <p></p>
                    <div class="row justify-content-center">
                        <div class="col-4 text-center">
                            <a href="mailto:<?= $expert->email ?>" target="_blank">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="col-4 text-center">
                            <a href="tel:<?= $expert->contact_no ?>" target="_blank">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="col-4 text-center">
                            <a href="<?= $expert->linkedin ?>" target="_blank">
                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</section>

<?php get_footer(); ?>

<script>
    jQuery(document).ready(function ($) {
        const nameSelector = $('#name-search')

        function filterExperts() {

            const locationFilter = $('#location-filter').val();
            const industryFilter = $('#industry-filter').val();
            const nameSearch = nameSelector.val().toLowerCase();

            $('.expert-item').each(function () {
                const location = $(this).data('location').toString();
                const industry = $(this).data('industry').toString();
                const name = $(this).find('h3').text().toLowerCase();

                let show = true;

                if (locationFilter && location !== locationFilter) {
                    show = false;
                }
                if (industryFilter && industry !== industryFilter) {
                    show = false;
                }
                if (nameSearch && name.indexOf(nameSearch) === -1) {
                    show = false;
                }

                if (show) {
                    $(this).fadeIn();
                } else {
                    $(this).fadeOut();
                }
            });
        }

        filterExperts();

        $('#location-filter, #industry-filter, #name-search').on('change keyup', function () {
            filterExperts();
        });

        nameSelector.closest('form').submit(function (e) {
            e.preventDefault();
            filterExperts();
        });
    });
</script>
